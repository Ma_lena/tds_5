// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_5 : ModuleRules
{
	public TDS_5(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate", "Niagara" });
    }
}
