// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_5.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_5, "TDS_5" );

DEFINE_LOG_CATEGORY(LogTDS_5)
 