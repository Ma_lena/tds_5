// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_5GameMode.generated.h"

UCLASS(minimalapi)
class ATDS_5GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_5GameMode();
};



