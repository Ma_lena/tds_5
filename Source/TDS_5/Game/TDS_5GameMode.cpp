// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_5GameMode.h"
#include "TDS_5PlayerController.h"
#include "TDS_5/Character/TDS_5Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS_5GameMode::ATDS_5GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_5PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/Game/BP_PlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}